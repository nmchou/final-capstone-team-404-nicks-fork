package com.techelevator.model;

public class RouteDetails {
    private int userRouteDetailsId;
    private int userRouteId;
    private String beginStopId;
    private String endStopId;
    private int sequenceId;

    public int getUserRouteDetailsId() {
        return userRouteDetailsId;
    }

    public void setUserRouteDetailsId(int userRouteDetailsId) {
        this.userRouteDetailsId = userRouteDetailsId;
    }

    public int getUserRouteId() {
        return userRouteId;
    }

    public void setUserRouteId(int userRouteId) {
        this.userRouteId = userRouteId;
    }

    public String getBeginStopId() {
        return beginStopId;
    }

    public void setBeginStopId(String beginStopId) {
        this.beginStopId = beginStopId;
    }
    public String getEndStopId() {
        return endStopId;
    }

    public void setEndStopId(String endStopId) {
        this.endStopId = endStopId;
    }

    public int getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
    }


}
