package com.techelevator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCRouteDetailsDAO implements RouteDetailsDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCRouteDetailsDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void save(RouteDetails routeDetails) {
        int userRouteId = routeDetails.getUserRouteId();
        String beginStopId = routeDetails.getBeginStopId();
        String endStopId = routeDetails.getEndStopId();
        int sequenceId = routeDetails.getSequenceId();
        String sqlInsertRouteDetails = "INSERT INTO route_details(user_route_id, begin_stop_id, end_stop_id, sequence_id) " +
                "VALUES (?,?,?,?)";
        jdbcTemplate.update(sqlInsertRouteDetails, userRouteId, beginStopId, endStopId, sequenceId);
    }

    @Override
    public RouteDetails determineStopIds(String line, String beginStop, String endStop, int sequenceCounter, int userRouteId) {

        RouteDetails routeDetails = new RouteDetails();

        String sqlSelectBeginStopId = "SELECT DISTINCT CASE WHEN " +
                "(SELECT ls.sequence_num FROM stop s JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? AND l.direction='N') < " +
                "(SELECT ls.sequence_num from stop s " +
                "JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? and l.direction='N') " +
                "THEN (SELECT s.id FROM stop s JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? AND l.direction='N') " +
                "ELSE (SELECT s.id FROM stop s " +
                "JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? AND l.direction='S') " +
                "END FROM line;";
        SqlRowSet result1 = jdbcTemplate.queryForRowSet(sqlSelectBeginStopId, beginStop, line, endStop, line, beginStop, line, beginStop, line);
        while (result1.next()) {
            routeDetails.setBeginStopId(result1.getString("id"));
        }

        String sqlSelectEndStopId = "SELECT DISTINCT CASE WHEN " +
                "(SELECT ls.sequence_num FROM stop s JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? AND l.direction='N') < " +
                "(SELECT ls.sequence_num from stop s " +
                "JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? and l.direction='N') " +
                "THEN (SELECT s.id FROM stop s JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? AND l.direction='N') " +
                "ELSE (SELECT s.id FROM stop s " +
                "JOIN line_stops ls ON ls.stop_id=s.id JOIN line l ON l.id=ls.line_id " +
                "WHERE s.stop_name=? AND l.name=? AND l.direction='S') " +
                "END FROM line;";
        SqlRowSet result2 = jdbcTemplate.queryForRowSet(sqlSelectEndStopId, beginStop, line, endStop, line, endStop, line, endStop, line);
        while (result2.next()) {
            routeDetails.setEndStopId(result2.getString("id"));
        }

        routeDetails.setUserRouteId(userRouteId);
        routeDetails.setSequenceId(sequenceCounter);
        return routeDetails;
    }

    @Override
    public List<String> getRouteIdsforRouteNames(String beginStop, String endStop) {
        List<String> stopIds = new ArrayList<>();
        List<String> nIds = new ArrayList<>();
        List<String> sIds = new ArrayList<>();

        String sqlSelectEndStopId = "select id from stop " +
                "where stop_name like '%?%' or stop_name like '%?%'";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectEndStopId, beginStop, endStop);

        while (result.next()) {
            String id = result.getString("id");
            if (id.substring(id.length() - 1).equals("N")) {
                nIds.add(id);
            } else if (id.substring(id.length() - 1).equals("S")) {
                sIds.add(id);
            } else stopIds.add(id);
        }
        int beginStopInt = Integer.parseInt(stopIds.get(0));
        int endStopInt = Integer.parseInt(stopIds.get(1));

        if (beginStopInt < endStopInt) {
            return sIds;
        } else
            return nIds;
    }

    @Override
    public List<RouteDetails> getRouteDetailsForRoute(int route_id) {
        String sqlSelectEndStopId = "select * from route_details where user_route_id = ? " +
                "order by sequence_id asc";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectEndStopId, route_id);

        List<RouteDetails> routeDetails = new ArrayList<>();

        while (result.next()) {
            routeDetails.add(mapRowToRoute(result));
        }

        return routeDetails;
    }

    private RouteDetails mapRowToRoute(SqlRowSet row) {
        RouteDetails route = new RouteDetails();
        route.setBeginStopId(row.getString("begin_stop_id"));
        route.setEndStopId(row.getString("end_stop_id"));
        route.setEndStopId(row.getString("sequence_id"));
        route.setUserRouteId(row.getInt("user_route_id"));
        route.setUserRouteDetailsId(row.getInt("id"));
        return route;
    }


}

