package com.techelevator.model;

import java.util.List;

public interface UserDAO {

	public void saveUser(String userName, String password, String role);

	public boolean searchForUsernameAndPassword(String userName, String password);

	public void updatePassword(String userName, String password);

	public Object getUserByUserName(String userName);

	public Boolean isUserLockedOut(String userName);

	public List<String> getAllUsers();

	public int getUserIdForUserName(String userName);

}
