package com.techelevator.model;

import java.util.List;

public interface UserRouteDAO {
    public int save(UserRoute route);
    public List<UserRoute> getAllRoutesForAUser(int user);
    public int getRouteIdForName(String routeName);
}
