package com.techelevator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCStopDAO implements StopDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCStopDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Stop> getAllStops() {
        List<Stop> allStops = new ArrayList<>();
        String sqlSelectValidStops = "SELECT * FROM stop";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectValidStops);
        while(results.next()) {
            allStops.add(mapRowToStop(results));
        }
        return allStops;
    }

    @Override
    public List<Stop> getUniqueStops() {
        List<Stop> allStops = new ArrayList<>();
        String sqlSelectValidStops = "SELECT * FROM stop WHERE parent_station is null";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectValidStops);
        while(results.next()) {
            allStops.add(mapRowToStop(results));
        }
        return allStops;
    }

    @Override
    public List<String> getAvailableStopsByLine(String line) {
        List<String> validStops = new ArrayList<>();

        String sqlSelectValidStops = "SELECT stop_name from line l " +
                "FULL OUTER JOIN line l2 on l.name = l2.name and l2.direction = 'S' " +
                "JOIN line_stops ls on l.id = ls.line_id OR (l2.id = ls.line_id AND l.id IS NULL) " +
                "JOIN stop s on ls.stop_id = s.id " +
                "WHERE l.name = ? AND l.direction = 'N' " +
                "GROUP BY stop_name " +
                "ORDER BY MIN(sequence_num) ASC";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectValidStops, line);
        while(results.next()) {
            validStops.add(results.getString("stop_name"));
        }
        return validStops;
    }

    @Override
    public List<String> getAvailableLinesByStop(String stopName) {
        List<String> validLines = new ArrayList<>();

        String sqlSelectValidStops = "SELECT DISTINCT l.name FROM line l " +
                "JOIN line_stops ls ON ls.line_id=l.id " +
                "JOIN stop s ON s.id=ls.stop_id " +
                "WHERE s.stop_name LIKE ? ORDER BY l.name";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectValidStops, stopName);
        while(results.next()) {
            validLines.add(results.getString("name"));
        }
        return validLines;
    }

    @Override
    public List<String> getTrainNames() {
        List<String> trainNames = new ArrayList<>();
        String sqlSelectFeed_ID = "SELECT DISTINCT name FROM line ORDER BY name";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectFeed_ID);
        while (results.next()) {
            trainNames.add(results.getString("name"));
        }
        return trainNames;
    }

    private Stop mapRowToStop(SqlRowSet row) {
        Stop stop = new Stop();
        stop.setStopId(row.getString("id"));
        stop.setStopName(row.getString("stop_name"));
        stop.setStopLatitude(row.getFloat("stop_lat"));
        stop.setStopLongitude(row.getFloat("stop_lon"));
        stop.setLocationType(row.getInt("location_type"));
        stop.setParentStation(row.getString("parent_station"));
        return stop;
    }
}
