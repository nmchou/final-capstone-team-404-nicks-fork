package com.techelevator.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.transit.realtime.GtfsRealtime;
import com.google.transit.realtime.GtfsRealtime.FeedMessage;
import com.google.transit.realtime.GtfsRealtimeNYCT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ProtoBuffMTADataDAO implements MTADataDAO {

    @Autowired
    private FeedDAO feedDao;

    @Autowired
    private StopDAO stopDao;

    private ExtensionRegistry registry = ExtensionRegistry.newInstance();
    String urlString = "http://datamine.mta.info/mta_esi.php?key=1f3e13ef3094e54739b89f6fb28552f8&feed_id=";

    //@Autowired
    public ProtoBuffMTADataDAO() {
        registry.add(GtfsRealtimeNYCT.nyctFeedHeader);
        registry.add(GtfsRealtimeNYCT.nyctStopTimeUpdate);
        registry.add(GtfsRealtimeNYCT.nyctTripDescriptor);
        try {
            URL url = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public FeedMessage getFeedMessage(int mtaParameter) {
        registry.add(GtfsRealtimeNYCT.nyctFeedHeader);
        registry.add(GtfsRealtimeNYCT.nyctStopTimeUpdate);
        registry.add(GtfsRealtimeNYCT.nyctTripDescriptor);
        GtfsRealtime.FeedMessage feed = null;
        try {
            URL url = new URL(urlString + mtaParameter);
            feed = GtfsRealtime.FeedMessage.parseFrom(url.openStream(), registry);
        } catch (InvalidProtocolBufferException f) {
            f.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException d) {
            d.printStackTrace();
        }
        return feed;
    }

    public List<TrainStatus> getDelayedTrains() {

        List<Integer> feedIds = feedDao.getAllFeedIds();

        List<String> trainNames = stopDao.getTrainNames();

        List<TrainStatus> allTrainStatus = new ArrayList<TrainStatus>();
        for (String s : trainNames) {
            TrainStatus status = new TrainStatus();
            status.setTrainName(s);
            status.setTrainStatus("Not Available");
            allTrainStatus.add(status);
        }


        File f = new File("C:\\tmp\\logs");
        if (!f.exists()) {
            f.mkdir();
        }
        LocalDateTime dateTimeStamp = LocalDateTime.now();
        DateTimeFormatter formatted = DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmSS");
        String formattedDateTime = dateTimeStamp.format(formatted);
        String delayLog = formattedDateTime + "-Delay.txt";
        System.out.println("DelayLog: " + delayLog);
        File outputFile = new File("C:\\tmp\\logs", delayLog);
        String stringToPrint = "";
        String delayedTrainName = "";
        String delayedTrainDirection = "";

        try (PrintWriter writer = new PrintWriter(outputFile)) {
            for (int i = 0; i < feedIds.size(); i++) {
                ProtoBuffMTADataDAO dataDAO = new ProtoBuffMTADataDAO();
                GtfsRealtime.FeedMessage feed = dataDAO.getFeedMessage(feedIds.get(i));

                //Periodically feed has no data so must check for null message below
                if (feed == null) {
                    stringToPrint = "WARNING!!!! >> Feed ID #" + feedIds.get(i) + " is returning a NULL message body!";
                    writer.println(stringToPrint);
                    System.out.println(stringToPrint);
                } else {
                    stringToPrint = "FEED ID: " + feedIds.get(i);
                    writer.println(stringToPrint);
                    System.out.println("FEED ID: " + feedIds.get(i));
                    String headerInfo = feed.getHeader().toString();
                    Pattern ptn = Pattern.compile("route_id:\\s\"(\\w{1,2})\"");
                    Matcher mtch = ptn.matcher(headerInfo);
                    while (mtch.find()) {
                        stringToPrint = "Checking the following line(s): " + mtch.group();
                        System.out.println(stringToPrint);
                        writer.println(stringToPrint);
                        String lineName = mtch.group(1);
                        for (TrainStatus t : allTrainStatus) {
                            if (t.getTrainName().equals(lineName)) {
                                t.setTrainStatus("On time");
                            }
                        }
                    }

                    for (GtfsRealtime.FeedEntity entity : feed.getEntityList()) {
                        GtfsRealtime.Alert alert = entity.getAlert();
                        for (int j = 0; j < alert.getInformedEntityList().size(); j++) {
                            stringToPrint = "DELAYED TRIP ID: " + alert.getInformedEntity(j).getTrip().getTripId();
                            writer.println(stringToPrint);
                            System.out.println(stringToPrint);
                            String delayedTripId = alert.getInformedEntity(j).getTrip().getTripId();
                            ptn = Pattern.compile("_......");
                            mtch = ptn.matcher(delayedTripId);
                            while (mtch.find()) {
                                stringToPrint = "REGEX Trip ID INFO : " + mtch.group();
                                System.out.println(stringToPrint);
                                writer.println(stringToPrint);
                                delayedTrainName = mtch.group().substring(1, 2);
                                System.out.println("Delayed train: " + delayedTrainName);
                                delayedTrainDirection = mtch.group().substring(4, 5);
                                System.out.println("Traveling in Direction: " + delayedTrainDirection);
                                for (TrainStatus t : allTrainStatus) {
                                    if (t.getTrainName().equals(delayedTrainName)) {
                                        t.setTrainStatus("Delays");
                                        t.setTrainDirection(delayedTrainDirection);
                                    }
                                }
                            }

                        }
                    }

                }
            }
            writer.flush();
        } catch (FileNotFoundException fnf) {
            System.out.println("Unable to create file: " + outputFile.getName());
        }

        return allTrainStatus;
    }
}
