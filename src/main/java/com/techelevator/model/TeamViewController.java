package com.techelevator.model;
import com.techelevator.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TeamViewController {

    @Autowired
    private TeamDAO teamDAO;

    @Autowired
    private TeamViewRouteDAO teamViewRouteDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private UserRouteDAO userRouteDAO;

    @RequestMapping(path={"/createNewTeam"}, method = RequestMethod.GET)
    public String displayUsers(HttpSession session) {

        List<TeamViewBuilder> teamViewBuilderList = new ArrayList<>();
        session.setAttribute("teamViewBuilderList",teamViewBuilderList);

        List<String> listOfUsers = new ArrayList<>();
        listOfUsers = userDAO.getAllUsers();
        session.setAttribute("listOfUsers",listOfUsers);
        return "createNewTeam";
    }



    @RequestMapping(path={"/createNewTeam"}, method = RequestMethod.POST)
    public String displayUsers(@RequestParam String userName, HttpSession session) {

        int userId = userDAO.getUserIdForUserName(userName);

        session.setAttribute("userId", userId);

        return "redirect:/createNewTeamAddRoute";
    }
    @RequestMapping(path={"/addUsersToTeam"}, method = RequestMethod.GET)
    public String displayTeamsAndUsers(HttpSession session){
        List<String> listOfUsers = new ArrayList<>();
        User user = (User)session.getAttribute("currentUser");
        listOfUsers = userDAO.getAllUsers();
        session.setAttribute("listOfUsers",listOfUsers);
        session.setAttribute("userTeams", teamDAO.getAllTeamsForAUser(user.getUserId()));

        return"addUsersToTeam";
    }

    @RequestMapping(path={"/addUsersToTeam"}, method = RequestMethod.POST)
    public String postUserTeam(HttpSession session, @RequestParam String userName, @RequestParam String userTeam) {
        int userId = userDAO.getUserIdForUserName(userName);

        session.setAttribute("userTeam", userTeam);
        session.setAttribute("userId", userId);

    return "redirect:/addUserRouteToTeam";
    }


    @RequestMapping(path={"/addUserRouteToTeam"}, method = RequestMethod.GET)
    public String displayUserRoutes(HttpSession session){

        List<UserRoute> userRoutes = userRouteDAO.getAllRoutesForAUser((Integer)session.getAttribute("userId"));
        session.setAttribute("userRoutes", userRoutes);

        return"addUserRouteToTeam";
    }

    @RequestMapping(path={"/addUserRouteToTeam"}, method = RequestMethod.POST)
    public String postUserRoute(HttpSession session, @RequestParam String userRoute){
        int  userRouteId = userRouteDAO.getRouteIdForName(userRoute);

        int teamId = teamDAO.getTeamIdForName((String)session.getAttribute("userTeam"));

        TeamViewRoute teamViewRoute = new TeamViewRoute();
        teamViewRoute.setUserRouteId(userRouteId);
        teamViewRoute.setTeamViewId(teamId);
        teamViewRouteDAO.save(teamViewRoute);
        return"redirect:/users/userHome";
    }


    @RequestMapping(path = {"/createNewTeamAddRoute"}, method = RequestMethod.GET)
    public String displayRouteStops(HttpSession session, ModelMap map) {
        List<UserRoute> userRoutes = userRouteDAO.getAllRoutesForAUser((Integer)session.getAttribute("userId"));
        map.put("userRoutes", userRoutes);
        return "createNewTeamAddRoute";
    }

    @RequestMapping(path="/createNewTeamAddRoute", method = RequestMethod.POST)
    public String captureUserRoute(@RequestParam String routeName, HttpSession session) {

        session.setAttribute("routeName", routeName);
        return "redirect:/teamViewSummary";
    }

    @RequestMapping(path="/teamViewSummary", method = RequestMethod.GET)
    public String getUserRoutes1(HttpSession session){
        TeamViewBuilder teamViewBuilder = new TeamViewBuilder();

        int routeId = userRouteDAO.getRouteIdForName((String)session.getAttribute("routeName"));
        teamViewBuilder.setUserRouteId(routeId);

        List<TeamViewBuilder> teamViewBuilderList = (ArrayList<TeamViewBuilder>)session.getAttribute("teamViewBuilderList");
        teamViewBuilderList.add(teamViewBuilder);

        return "teamViewSummary";
    }

    @RequestMapping(path = "/teamViewSummary", method = RequestMethod.POST)
    public String confirmTeam(@RequestParam String teamViewName,
                               HttpSession session) {

        User user = (User)session.getAttribute("currentUser");

        Team team = new Team();
        team.setViewName(teamViewName);//currently no validation on possible duplicate
        team.setAccess_level(1);//have not implemented any features around this field
        team.setOwner_id(user.getUserId());

        int teamViewId = teamDAO.save(team);

        List<TeamViewBuilder> teamViewBuilderList = (ArrayList<TeamViewBuilder>)session.getAttribute("teamViewBuilderList");

        for(TeamViewBuilder userRoute : teamViewBuilderList) {

            int userRouteId = userRoute.getUserRouteId();
            TeamViewRoute teamViewRoute = new TeamViewRoute();
            teamViewRoute.setUserRouteId(userRouteId);
            teamViewRoute.setTeamViewId(teamViewId);
            teamViewRouteDAO.save(teamViewRoute);
        }
        return "redirect:/users/userHome";
    }

}
