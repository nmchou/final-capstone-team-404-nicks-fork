package com.techelevator.model;

import com.google.transit.realtime.GtfsRealtime;

import java.util.List;

public interface MTADataDAO {
    public List<TrainStatus> getDelayedTrains();
    public GtfsRealtime.FeedMessage getFeedMessage(int mtaParameter);
}
