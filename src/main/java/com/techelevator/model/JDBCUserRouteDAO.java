package com.techelevator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCUserRouteDAO implements UserRouteDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCUserRouteDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int save(UserRoute route) {
        String sqlInsertReview = "INSERT INTO user_route (route_name, app_user_id, access_level) VALUES (?,?,?) RETURNING id";

        try {
            int userRouteId = jdbcTemplate.queryForObject(sqlInsertReview, int.class, route.getRouteName(), route.getAppUserId(), route.getAccessLevel());
            return userRouteId;
        }
        catch (DataAccessException dae){
            System.out.println(dae);
            return -1;
        }
    }

    @Override
    public List<UserRoute> getAllRoutesForAUser(int user){
        List<UserRoute> userRoutes = new ArrayList<>();

        String sqlSelectEndStopId = "select * from user_route where app_user_id = ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectEndStopId, user);
        while(result.next()) {
            userRoutes.add(mapRowToRoute(result));
        }
        return userRoutes;
    }
/*
    @Override
    public List<UserRoute> getAllRoutesFor(int user){
        List<UserRoute> userRoutes = new ArrayList<>();

        String sqlSelectEndStopId = "select * from user_route where app_user_id = ?";
        SqlRowSet result = jdbcTemplate.queryForRowSet(sqlSelectEndStopId, user);
        while(result.next()) {
            userRoutes.add(mapRowToRoute(result));
        }
        return userRoutes;
    }*/

    @Override
    public int getRouteIdForName(String routeName){
        String sqlSelectEndStopId = "select id from user_route where route_name = ?";

        Integer results = jdbcTemplate.queryForObject(sqlSelectEndStopId, Integer.class, routeName);
        int stopId = results;

        return stopId;
    }


    private UserRoute mapRowToRoute(SqlRowSet row) {
        UserRoute route  = new UserRoute();
        route.setRouteName(row.getString("route_name"));
        route.setAccessLevel(row.getInt("access_level"));
        route.setAppUserId(row.getInt("app_user_id"));
        route.setUserRouteId(row.getInt("id"));

        return route;
    }
}
