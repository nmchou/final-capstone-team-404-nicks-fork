package com.techelevator.model;

import java.util.List;

public interface TeamDAO {

    public int save(Team teamView);

    public List<Team> getAllTeamsForAUser(int userId);

    public int getTeamIdForName(String name);
}