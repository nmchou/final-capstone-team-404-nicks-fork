package com.techelevator.model;

import java.util.List;

public class RouteDetailsForRoute {
    private int userRouteId;
    private String routeName;
    private int appUserId;
    private int accessLevel;

    public RouteDetailsForRoute(int userRouteId, String routeName, int appUserId, int accessLevel, List<RouteDetails> routeDetails) {
        this.userRouteId = userRouteId;
        this.routeName = routeName;
        this.appUserId = appUserId;
        this.accessLevel = accessLevel;
        this.routeDetails = routeDetails;
    }

    public List<RouteDetails> getRouteDetails() {
        return routeDetails;
    }

    public void setRouteDetails(List<RouteDetails> routeDetails) {
        this.routeDetails = routeDetails;
    }

    private List<RouteDetails> routeDetails;

    public int getUserRouteId() {
        return userRouteId;
    }

    public void setUserRouteId(int userRouteId) {
        this.userRouteId = userRouteId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public int getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(int appUserId) {
        this.appUserId = appUserId;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }


}
