package com.techelevator.model;

import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCTeamDAO implements TeamDAO{

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCTeamDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public int save(Team teamView) {

        String sqlInsertTeamView = "INSERT INTO team_view (view_name, owner_id, access_level) VALUES (?,?,?) RETURNING id";

        try {
            int teamViewId = jdbcTemplate.queryForObject(sqlInsertTeamView, int.class, teamView.getViewName(), teamView.getOwner_id(), teamView.getAccess_level());
            return teamViewId;
        }
        catch (DataAccessException dae){
            System.out.println(dae);
            return -1;
        }


    }

    @Override
    public List<Team> getAllTeamsForAUser(int userId){
        String sqlGetTeamsForUserId = "select * from team_view where owner_id = ?";
        List<Team> userTeams = new ArrayList<>();
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetTeamsForUserId, userId);
        while(results.next()) {
            userTeams.add(mapRowToTeam(results));
            }
        return userTeams;
    }

    @Override
    public int getTeamIdForName(String name){
        String sqlGetTeamsForUserId = "select id from team_view where view_name = ?";

        Integer results = jdbcTemplate.queryForObject(sqlGetTeamsForUserId, Integer.class, name);

        return results;
    }



    private Team mapRowToTeam(SqlRowSet row) {
        Team team = new Team();
        team.setId(row.getInt("id"));
        team.setAccess_level(row.getInt("access_level"));
        team.setOwner_id(row.getInt("owner_id"));
        team.setViewName(row.getString("view_name"));


        return team;
    }


}
