package com.techelevator.model;

import java.util.List;

public interface FeedDAO {
    public List<Integer> getAllFeedIds();
}
