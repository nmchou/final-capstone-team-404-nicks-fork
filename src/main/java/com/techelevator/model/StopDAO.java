package com.techelevator.model;

import java.util.List;

public interface StopDAO {
    public List<Stop> getAllStops();
    public List<String> getAvailableStopsByLine(String line);
    public List<String> getAvailableLinesByStop(String stopName);
    public List<String > getTrainNames();
    List<Stop> getUniqueStops();
}
