package com.techelevator.model;

public class UserRoute {
    private int userRouteId;
    private String routeName;
    private int appUserId;
    private int accessLevel;

    public int getUserRouteId() {
        return userRouteId;
    }

    public void setUserRouteId(int userRouteId) {
        this.userRouteId = userRouteId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public int getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(int appUserId) {
        this.appUserId = appUserId;
    }

    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }
}
