package com.techelevator.model;

public class Stop {
    private String stopId;
    private String stopName;
    private float stopLatitude;
    private float stopLongitude;
    private int locationType;
    private String parentStation;

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public float getStopLatitude() {
        return stopLatitude;
    }

    public void setStopLatitude(float stopLatitude) {
        this.stopLatitude = stopLatitude;
    }

    public float getStopLongitude() {
        return stopLongitude;
    }

    public void setStopLongitude(float stopLongitude) {
        this.stopLongitude = stopLongitude;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public String getParentStation() {
        return parentStation;
    }

    public void setParentStation(String parentStation) {
        this.parentStation = parentStation;
    }
}
