package com.techelevator.model;

public class TrainStatus {
    private String trainStatus;
    private String trainName;
    private String trainDirection;

    public  String getTrainStatus() {
        return trainStatus;
    }

    public void setTrainStatus(String trainStatus) {
        this.trainStatus = trainStatus;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getTrainDirection() {
        return trainDirection;
    }

    public void setTrainDirection(String trainDirection) {
        this.trainDirection = trainDirection;
    }
}