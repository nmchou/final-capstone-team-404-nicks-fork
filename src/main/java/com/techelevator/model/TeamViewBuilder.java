package com.techelevator.model;

public class TeamViewBuilder {

    private int userRouteId;

    public int getUserRouteId() {
        return userRouteId;
    }

    public void setUserRouteId(int userRouteId) {
        this.userRouteId = userRouteId;
    }
}
