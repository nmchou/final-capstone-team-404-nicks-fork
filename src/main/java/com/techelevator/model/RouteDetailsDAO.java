package com.techelevator.model;

import java.util.List;

public interface RouteDetailsDAO {
    public void save(RouteDetails routeDetails);
    public RouteDetails determineStopIds(String line, String beginStop, String endStop, int sequenceCounter, int userRouteId);
    //public RouteDetails saveRoute(String line, String beginStop, String endStop, int sequenceCounter, int userRouteId);
    public List<String> getRouteIdsforRouteNames(String beginStop, String endStop);
    public List<RouteDetails> getRouteDetailsForRoute(int route_id);}
