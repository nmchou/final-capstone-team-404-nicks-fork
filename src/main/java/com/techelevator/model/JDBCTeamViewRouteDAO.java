package com.techelevator.model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class JDBCTeamViewRouteDAO implements TeamViewRouteDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCTeamViewRouteDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void save(TeamViewRoute teamViewRoute) {
        int teamViewId = teamViewRoute.getTeamViewId();
        int userRouteId = teamViewRoute.getUserRouteId();

        String sqlInsertTeamViewRoute = "INSERT INTO team_view_x_user_route(team_view_id, user_route_id) " +
                "VALUES (?,?)";
        jdbcTemplate.update(sqlInsertTeamViewRoute,teamViewId,userRouteId);

    }
}
