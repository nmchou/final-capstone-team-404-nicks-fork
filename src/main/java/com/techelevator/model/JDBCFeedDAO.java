package com.techelevator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class JDBCFeedDAO implements FeedDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCFeedDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Integer> getAllFeedIds() {
        List<Integer> feed_id = new ArrayList<>();
        String sqlSelectFeed_ID = "SELECT DISTINCT feed_id FROM line ORDER BY feed_id";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectFeed_ID);
        while (results.next()) {
            feed_id.add(results.getInt("feed_id"));
        }
        return feed_id;
    }

    // Next method - get distinct feed_id or particular train.
    // Add direction to limit result to one.
    // SELECT feed_id FROM line WHERE name = 'G' AND direction = 'N';
}
