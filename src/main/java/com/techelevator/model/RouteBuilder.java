package com.techelevator.model;

public class RouteBuilder {

    private String line;
    private String beginStopName;
    private String endStopName;

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getBeginStopName() {
        return beginStopName;
    }

    public void setBeginStopName(String beginStopName) {
        this.beginStopName = beginStopName;
    }

    public String getEndStopName() {
        return endStopName;
    }

    public void setEndStopName(String endStopName) {
        this.endStopName = endStopName;
    }
}
