package com.techelevator.model;

public class TeamViewRoute {

    int teamViewId;
    int userRouteId;

    public int getUserRouteId() {
        return userRouteId;
    }

    public void setUserRouteId(int userRouteId) {
        this.userRouteId = userRouteId;
    }

    public int getTeamViewId() {
        return teamViewId;
    }

    public void setTeamViewId(int teamViewId) {
        this.teamViewId = teamViewId;
    }
}
