package com.techelevator.controller;

import com.techelevator.model.*;
import com.techelevator.view.RenderedSubway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RouteController {

    @Autowired
    private UserRouteDAO userRouteDAO;

    @Autowired
    private RouteDetailsDAO routeDetailsDAO;

    @Autowired
    private StopDAO stopDAO;

    public void showPreviousSelections(ModelMap userSelections, HttpSession session) {

        List<RouteBuilder> routeBuilderList = (ArrayList<RouteBuilder>) session.getAttribute("routeBuilderList");

        List<RouteBuilder> completedRoutes = new ArrayList<>();
        userSelections.put("currentRoute", routeBuilderList.get(routeBuilderList.size() - 1));
        if (routeBuilderList.size() > 1) {

            for (int i = 0; i < routeBuilderList.size() - 1; i++) {
                completedRoutes.add(routeBuilderList.get(i));
            }
            userSelections.put("userSelections", completedRoutes);
        }
    }

    @RequestMapping(path = {"/createRouteLine"}, method = RequestMethod.GET)
    public String displayLinesAndStops(HttpSession session, ModelMap userSelections) {

        String lastStop = "%";

        if (session.getAttribute("routeBuilderList") == null) {
            List<RouteBuilder> routeBuilderList = new ArrayList<RouteBuilder>();

            routeBuilderList.add(new RouteBuilder());
            session.setAttribute("routeBuilderList", routeBuilderList);

        } else {
            List<RouteBuilder> routeBuilderList = (ArrayList<RouteBuilder>) session.getAttribute("routeBuilderList");

            RouteBuilder routeBuilder = routeBuilderList.get(routeBuilderList.size() - 1);

            if (routeBuilder.getLine() != null) {
                RouteBuilder newRouteBuilder = new RouteBuilder();
                newRouteBuilder.setBeginStopName(routeBuilder.getEndStopName());
                routeBuilderList.add(newRouteBuilder);

            }

            showPreviousSelections(userSelections, session);
            lastStop = (String) session.getAttribute("lastStopPicked");

        }
        List<String> listOfTrains;
        listOfTrains = stopDAO.getAvailableLinesByStop(lastStop);
        List<RenderedSubway> renderedListOfTrains = new ArrayList<>();
        for (String s : listOfTrains) {
            RenderedSubway renderedSubway = new RenderedSubway();
            renderedSubway.setLine(s);
            renderedListOfTrains.add(renderedSubway);
        }
        userSelections.put("listOfTrains", renderedListOfTrains);
        return "createRouteLine";
    }

    @RequestMapping(path = "/createRouteLine", method = RequestMethod.POST)
    public String captureLineInput(@RequestParam String trainLine,
                                   HttpSession session) {

        List<String> listOfStops;

        List<RouteBuilder> routeBuilderList = (ArrayList<RouteBuilder>) session.getAttribute("routeBuilderList");
        RouteBuilder routeBuilder = routeBuilderList.get(routeBuilderList.size() - 1);
        routeBuilder.setLine(trainLine);

        listOfStops = stopDAO.getAvailableStopsByLine(trainLine);
        session.setAttribute("listOfStops", listOfStops);

        return "redirect:/createRouteStop";
    }

    @RequestMapping(path = {"/createRouteStop"}, method = RequestMethod.GET)
    public String displayRouteStops(HttpSession session, ModelMap userSelections) {
        showPreviousSelections(userSelections, session);

        return "createRouteStop";
    }


    @RequestMapping(path = "/createRouteStop", method = RequestMethod.POST)
    public String captureStopInput(@RequestParam boolean imDone,
                                   @RequestParam String beginStop,
                                   @RequestParam String endStop,
                                   HttpSession session) {

        List<RouteBuilder> routeBuilderList = (ArrayList<RouteBuilder>) session.getAttribute("routeBuilderList");
        session.setAttribute("lastStopPicked", endStop);
        int routeBuilderSize = routeBuilderList.size();

        if (routeBuilderSize > 0) {

            RouteBuilder routeBuilder = routeBuilderList.get(routeBuilderSize - 1);
            routeBuilder.setBeginStopName(beginStop);
            routeBuilder.setEndStopName(endStop);
        }

        if (imDone) {
            return "redirect:/tripsummary";
        } else {
            return "redirect:/createRouteLine";
        }
    }

//    @RequestMapping(path = "/tripsummary", method = RequestMethod.GET)
//    public String displayRouteSummary(HttpSession session) {
//
//        if (session.getAttribute("routeDetails") == null) {
//            return "redirect:/index";
//        }
//        return "tripsummary";
//    }

    @RequestMapping(path = "/tripsummary", method = RequestMethod.POST)
    public String confirmRoute(@RequestParam String routeName,
                               @RequestParam int accessLevel,
                               HttpSession session) {
        int sequenceCounter = 0;

        User user = (User) session.getAttribute("currentUser");
        UserRoute userRoute = new UserRoute();
        userRoute.setAppUserId(user.getUserId());
        userRoute.setRouteName(routeName); //currently no validation on possible duplicate
        userRoute.setAccessLevel(accessLevel);
        int userRouteId = userRouteDAO.save(userRoute);

        List<RouteBuilder> routeBuilderList = (ArrayList<RouteBuilder>) session.getAttribute("routeBuilderList");

        for (int i = 0; i < routeBuilderList.size(); i++) {
            //for(RouteBuilder routeSegment : routeBuilderList) {

            RouteBuilder routeSegment = routeBuilderList.get(i);
            sequenceCounter++;
            String theLine = routeSegment.getLine();
            String beginStop = routeSegment.getBeginStopName();
            String endStop = routeSegment.getEndStopName();
            System.out.println("User route ID = " + userRouteId);
            RouteDetails routeDetails = routeDetailsDAO.determineStopIds(theLine, beginStop, endStop, sequenceCounter, userRouteId);

            routeDetailsDAO.save(routeDetails);
        }
        return "redirect:/users/userHome"; // Send back to user home page
    }
}
