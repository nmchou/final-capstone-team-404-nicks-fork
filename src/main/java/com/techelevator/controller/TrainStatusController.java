package com.techelevator.controller;

import com.techelevator.model.MTADataDAO;
import com.techelevator.model.ProtoBuffMTADataDAO;
import com.techelevator.model.TrainStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
public class TrainStatusController {

    @Autowired
    private MTADataDAO mtaDataDao;

    @RequestMapping("/trainStatus")
    public String getTrainStatus (ModelMap map){
        List<TrainStatus> allTrainStatus= mtaDataDao.getDelayedTrains();
        map.put("allTrainStatus",allTrainStatus);
        return ("trainStatus");
    }
}
