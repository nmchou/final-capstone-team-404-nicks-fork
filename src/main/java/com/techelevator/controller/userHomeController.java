package com.techelevator.controller;

import com.techelevator.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class userHomeController {

    @Autowired
    private UserRouteDAO userRouteDAO;

    @Autowired
    private  TeamDAO teamDAO;

    @Autowired
    private RouteDetailsDAO routeDetailsDAO;

    @Autowired
    private MTADataDAO mtaDataDAO;

    @RequestMapping(path={"users/userHome"}, method = RequestMethod.GET)
    public String getUserRoutes(ModelMap modelMap,
     HttpSession session) {
        User user = (User)session.getAttribute("currentUser");
        modelMap.put("userRoutes", userRouteDAO.getAllRoutesForAUser(user.getUserId()));

        List<TrainStatus> allTrainStatus= mtaDataDAO.getDelayedTrains();
        modelMap.put("allTrainStatus",allTrainStatus);

        List<UserRoute> userRoutes = userRouteDAO.getAllRoutesForAUser(user.getUserId());
        List<RouteDetailsForRoute> routeDetailsForRoutes = new ArrayList<>();
        List<RouteDetails> routeDetails = new ArrayList<>();
        for(int i = 0; i < userRoutes.size(); i++){
             routeDetails.addAll(routeDetailsDAO.getRouteDetailsForRoute(userRoutes.get(i).getUserRouteId()));
            RouteDetailsForRoute routeDetailForRoute = new RouteDetailsForRoute(userRoutes.get(i).getUserRouteId(), userRoutes.get(i).getRouteName(),
                    userRoutes.get(i).getAppUserId(), userRoutes.get(i).getAccessLevel(), routeDetails);
            routeDetailsForRoutes.add(routeDetailForRoute);
        }


        modelMap.put("routeDetailsForRoute", routeDetailsForRoutes);
        modelMap.put("routeDetails", routeDetails);
        modelMap.put("userTeams", teamDAO.getAllTeamsForAUser(user.getUserId()));
        return "userHome";
    }
//
//    @RequestMapping(path={"users/userHome"}, method = RequestMethod.GET)
//    public String getUserTeams(ModelMap modelMap) {
//        modelMap.put("userTeams", teamDAO.getAllTeamsForAUser(1));
//        return "userHome";
//    }



}
