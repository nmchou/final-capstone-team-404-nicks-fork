package com.techelevator.controller;

import com.techelevator.model.RouteBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TripsummaryController {

    @RequestMapping("/tripsummary")
    public String getTripsummary (HttpSession session, ModelMap map){
        List <RouteBuilder> routes = (ArrayList <RouteBuilder>)session.getAttribute("routeBuilderList");
        int lastRouteIndex = 0;
        if (routes != null && routes.size()>0){


            lastRouteIndex = routes.size()-1;
        }
        map.put("lastRouteIndex", lastRouteIndex);
        return ("tripsummary");
    }

}
