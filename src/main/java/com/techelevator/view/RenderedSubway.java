package com.techelevator.view;

import java.util.HashMap;
import java.util.Map;

public class RenderedSubway {

    private static Map<String, String> trainToStyle = new HashMap<>();
    static {
        trainToStyle.put("1","red-line");

        trainToStyle.put("2","red-line");

        trainToStyle.put("3","red-line");

        trainToStyle.put("4","green-line");

        trainToStyle.put("5","green-line");

        trainToStyle.put("6","green-line");

        trainToStyle.put("7","purple-line");

        trainToStyle.put("A","blue-line");

        trainToStyle.put("C","blue-line");

        trainToStyle.put("E","blue-line");

        trainToStyle.put("N","yellow-line");

        trainToStyle.put("Q","yellow-line");

        trainToStyle.put("R","yellow-line");

        trainToStyle.put("B","orange-line");

        trainToStyle.put("D","orange-line");

        trainToStyle.put("F","orange-line");

        trainToStyle.put("M","orange-line");

        trainToStyle.put("L","l-train");

        trainToStyle.put("J","brown-line");

        trainToStyle.put("Z","brown-line");

        trainToStyle.put("G","g-train");

        trainToStyle.put("S","s-train");

        trainToStyle.put("W","yellow-line");

        trainToStyle.put("GS","s-train");

        trainToStyle.put("H","blue-line");
    }

    private String line;
    private String style;

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
        this.setStyle(RenderedSubway.trainToStyle.get(line));
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

}

