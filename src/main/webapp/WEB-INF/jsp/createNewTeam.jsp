<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:set var="pageTitle" value="createNewTeam"/>
<c:url var="createNewTeam" value="/createNewTeam"/>


    <h1 class="title has-text-info"><span STYLE="font-family:Work Sans">Create a new Team View</span></h1>

    <form action="createNewTeam" method="POST">
        <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
        <h2 class="title has-text-info"><span STYLE="font-family:Work Sans;font-size: 24px">Select a User Name:</span></h2>
        <input class="input is-medium" type="text" autocomplete="off" id="namePicked" name="userName" list="user-list"/>
        <datalist id="user-list">
            <c:forEach items="${sessionScope.listOfUsers}" var="user">
                <option><c:out value="${user}"></c:out></option>
            </c:forEach>
        </datalist>
        <div class="field">
            <div class="control">
                <input id="submitbutton" type="submit" value="Select User" class="button is-link is-outlined"/>
            </div>
        </div>
    </form>

    <script type="application/javascript">
        $(document).ready(function () {

            var submitButton = $("#submitbutton");

            var textinput = $("#namePicked")

            var toggleButton = function () {
                if (textinput.val().length > 0) {
                    submitButton.css('display', '');
                } else {
                    submitButton.css('display', 'none');
                }
            }

            var toggleButton = function () {
                if (textinput.val().length > 0) {
                    submitButton2.css('display', '');
                } else {
                    submitButton2.css('display', 'none');
                }
            }

            textinput.change(function (event) {
                toggleButton();
            });

            toggleButton();

            $("#searchForm").hide();
            $("#submitbutton").click(function () {
                $("#searchForm").show();
            });
        })
    </script>

<c:import url="footer.jsp"></c:import>