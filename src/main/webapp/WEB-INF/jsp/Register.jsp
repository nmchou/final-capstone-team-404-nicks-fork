<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Annie Reardon
  Date: 4/30/2019
  Time: 4:43 PM
  To change this template use File | Settings | File Templates.
--%>
<c:import url="/WEB-INF/jsp/header.jsp" />

<script type="text/javascript">
    $(document).ready(function () {

        $("form").validate({

            rules : {
                userName : {
                    required : true
                },
                password : {
                    required : true
                }
            },
            messages : {
                confirmPassword : {
                    equalTo : "Passwords do not match"
                }
            },
            errorClass : "error"
        });
    });
</script>
<div class = "columns">
    <div class = column">
    </div>
    <div class = column">
        <c:url var="formAction" value="/register" />
        <form method="POST" action="${formAction}">
            <input type="hidden" name="destination" value="${param.destination}"/>
            <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
            <div class="field">
                <label for="createUserName" class="label">User Name: </label>
                <div class = "control">
                    <input type="text" id="createUserName" name="createUserName" placeHolder="Create User Name" class="input">
                </div>
            </div>
            <div class="field">
                <label for="password"class="label">Password: </label>
                <div class = "control">
                    <input type="password" id="password" name="password" placeHolder="Password" class="input">
                    <span class = "icon is-small is-left">
						<i class = "fas fa-lock"></i>
					</span>
                </div>
            </div>
            <div class = "control">
                <button type="submit" class="button is-primary">Login</button>
            </div>
        </form>
    </div>


    <div class = column">
    </div>
</div>
<c:url var="backgroundimg" value="/img/yellowpinkwall.jpg"></c:url>
<style>
    #outer-container {
        background-image: url("${backgroundimg}");
        background-size: contain;}

    .footer {
        background-color: unset;
        color: white;
    }
    #middlecolumn{
        background-color: white;
    }
</style>
<c:import url="footer.jsp"/>