<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:url var="addUserRouteToTeam" value="/addUserRouteToTeam"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form action="addUserRouteToTeam" method="POST">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
    <h2 class="title has-text-info"><span STYLE="font-family:Work Sans">Select a User's Route</span></h2>
    <input class="input is-medium" type="text" autocomplete="off" id="namePicked" name="userRoute" list="user-list"/>
    <datalist id="user-list">
        <c:forEach items="${sessionScope.userRoutes}" var="routes">
            <option><c:out value="${routes.routeName}"></c:out></option>
        </c:forEach>
    </datalist>
    <div class="field">
        <div class="control">
            <input id="submitbutton" type="submit" value="Submit" class="button is-link is-outlined"/>
        </div>
    </div>

    </main>
    <c:url var="backgroundimg" value="/img/bluetiles.jpg"></c:url>
    <style>
        .white {
            background-color: white;
        }
    </style>
</form>