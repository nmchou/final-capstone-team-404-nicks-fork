

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:url var="addUsersToTeam" value="/addUsersToTeam"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<form action="addUsersToTeam" method="POST">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
    <h2 class="title" style="color: #142b6f;">Select a User Name:</h2>
    <input class="input is-medium" type="text" autocomplete="off" id="namePicked" name="userName" list="user-list"/>
    <datalist id="user-list">
        <c:forEach items="${sessionScope.listOfUsers}" var="user">
            <option><c:out value="${user}"></c:out></option>
        </c:forEach>
    </datalist>
    <h2 class="title" style="color: #142b6f;">Select Team to add to:</h2>
    <input class="input is-medium" type="text" autocomplete="off" id="teamPicked" name="userTeam" list="team-list"/>
    <datalist id="team-list">
        <c:forEach items="${sessionScope.userTeams}" var="team">
            <option><c:out value="${team.viewName}"></c:out></option>
        </c:forEach>
    </datalist>
    <div class="field">
        <div class="control">
            <input id="submitbutton2" type="submit" value="Add User To Team" class="button is-link is-outlined"/>
        </div>
    </div>
</form>

<style>


    .white {
        background-color: white;
    }
</style>