<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:set var="pageTitle" value="createNewTeamAddRoute"/>
<c:url var="createNewTeamAddRoute" value="/createNewTeamAddRoute"/>


<form action="createNewTeamAddRoute" method="POST">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
    <h2 class="title has-text-info"><span STYLE="font-family:Work Sans">Add Route:</span></h2>
    <input class="input is-medium" type="text" autocomplete="off" id="routePicked" name="routeName" list="route-list"/>
    <datalist id="route-list">
        <c:forEach items="${userRoutes}" var="routes">
            <option><c:out value="${routes.routeName}"></c:out></option>
        </c:forEach>
    </datalist>
    <div class="field">
        <div class="control">
            <input id="submitbutton" type="submit" value="Route" class="button is-link is-outlined"/>
        </div>
    </div>
</form>

<script type="application/javascript">
    $(document).ready(function()  {

        var submitButton = $("#submitbutton");

        var textinput = $("#namePicked")

        var toggleButton = function(){
            if(textinput.val().length > 0){
                submitButton.css('display','');
            }    else {
                submitButton.css('display','none');
            }
        }


        textinput.change(function(event) {
            toggleButton();
        });

        toggleButton();

        $ ("#searchForm"). hide ();
        $("#submitbutton").click(function(){
            $("#searchForm").show();
        });
    })
</script>
<c:import url="footer.jsp"></c:import>