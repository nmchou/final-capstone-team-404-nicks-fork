<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:url var="createRouteStop" value="/createRouteStop"/>


<div class="columns">
    <div class="column">
        <c:url var="test" value="/img/templateyellowcircle.png"></c:url>
        <img src="${test}"/>
    </div>
    <div class="column">
        <c:import url="userSelections.jsp"></c:import>
        <form action="createRouteStop" method="POST">
            <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
            <c:forEach items="${sessionScope.routeBuilderList}" var="line" varStatus="status">
                <c:if test="${status.last}">
                    <c:choose>
                        <c:when test="${line.line == '1'}">
                            <label class="button column subway red-line"><input type="radio" name="trainLine"
                                                                                class="displayNone"/>1</label> </c:when>
                        <c:when test="${line.line == '2'}">
                            <label class="button column subway red-line"><input type="radio" name="trainLine" value="2"
                                                                                class="displayNone"/>2</label> </c:when>
                        <c:when test="${line.line == '3'}">
                            <label class="button column subway red-line"><input type="radio" name="trainLine" value="3"
                                                                                class="displayNone"/>3</label></c:when>
                        <c:when test="${line.line == '4'}">
                            <label class="button column subway green-line"><input type="radio" name="trainLine"
                                                                                  value="4"
                                                                                  class="displayNone"/>4</label> </c:when>
                        <c:when test="${line.line == '5'}">
                            <label class="button column subway green-line"><input type="radio" name="trainLine"
                                                                                  value="5"
                                                                                  class="displayNone"/>5</label> </c:when>
                        <c:when test="${line.line == '6'}">
                            <label class="button column subway green-line"><input type="radio" name="trainLine"
                                                                                  value="6"
                                                                                  class="displayNone"/>6</label></c:when>
                        <c:when test="${line.line == '7'}">
                            <label class="button column subway purple-line"><input type="radio" name="trainLine"
                                                                                   value="7"
                                                                                   class="displayNone"/>7</label></c:when>
                        <c:when test="${line.line == 'A'}">
                            <label class="button column subway blue-line"><input type="radio" name="trainLine" value="A"
                                                                                 class="displayNone"/>A</label></c:when>
                        <c:when test="${line.line == 'C'}">
                            <label class="button column subway blue-line"><input type="radio" name="trainLine" value="C"
                                                                                 class="displayNone"/>C</label></c:when>
                        <c:when test="${line.line == 'E'}">
                            <label class="button column subway blue-line"><input type="radio" name="trainLine" value="E"
                                                                                 class="displayNone"/>E</label></c:when>
                        <c:when test="${line.line == 'N'}">
                            <<label class="button column subway yellow-line"><input type="radio" name="trainLine" value="N" class="displayNone"/>N</label></c:when>
                        <c:when test="${line.line == 'Q'}">
                            <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                   value="Q"
                                                                                   class="displayNone"/>Q</label></c:when>
                        <c:when test="${line.line == 'R'}">
                            <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                   value="R"
                                                                                   class="displayNone"/>R</label></c:when>
                        <c:when test="${line.line == 'B'}">
                            <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                   value="B"
                                                                                   class="displayNone"/>B</label></c:when>
                        <c:when test="${line.line == 'D'}">
                            <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                   value="D"
                                                                                   class="displayNone"/>D</label></c:when>
                        <c:when test="${line.line == 'F'}">
                            <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                   value="F"
                                                                                   class="displayNone"/>F</label></c:when>
                        <c:when test="${line.line == 'M'}">
                            <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                   value="M"
                                                                                   class="displayNone"/>M</label></c:when>
                        <c:when test="${line.line == 'L'}">
                            <label class="button column subway l-train"><input type="radio" name="trainLine" value="L"
                                                                               class="displayNone"/>L</label></c:when>
                        <c:when test="${line.line == 'J'}">
                            <label class="button column subway brown-line"><input type="radio" name="trainLine"
                                                                                  value="J"
                                                                                  class="displayNone"/>J</label></c:when>
                        <c:when test="${line.line == 'Z'}">
                            <label class="button column subway brown-line"><input type="radio" name="trainLine"
                                                                                  value="Z"
                                                                                  class="displayNone"/>Z</label></c:when>
                        <c:when test="${line.line == 'G'}">
                            <label class="button column subway g-train"><input type="radio" name="trainLine" value="G"
                                                                               class="displayNone"/>G</label></c:when>
                        <c:when test="${line.line == 'S'}">
                            <label class="button column subway s-train"><input type="radio" name="trainLine" value="S"
                                                                               class="displayNone"/>S</label></c:when>
                    </c:choose>
                </c:if>
            </c:forEach>
            <h1 class="title">Select a Location</h1>
            <div class="timeline">
                <div class="timeline-item">
                    <div class="hero-body">
                        <div class="timeline-marker"></div>
                        <div class="timeline-content">
                            <c:choose>
                                <c:when test="${currentRoute.beginStopName==null}">
                                    <div class="control">

                                        <h2 class="title"></i> Enter a Starting Location</h2>
                                        <input class="input is-medium" autocomplete="off" type="text" list="start-list"
                                               name="beginStop"/>
                                        <datalist id="start-list">
                                            <c:forEach items="${sessionScope.listOfStops}" var="stop">
                                                <option><c:out value="${stop}"></c:out></option>
                                            </c:forEach>
                                        </datalist>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <input type="hidden" name="beginStop" value="${currentRoute.beginStopName}">
                                </c:otherwise>
                            </c:choose>

                        </div>
                    </div>
                </div>
                <div class="timeline-marker is-primary"></div>
                <div class="timeline-item">
                    <div class="hero-body">
                        <div class="timeline-content">
                            <div class="control">
                                <h2 class="title"></i> Enter a Destination</h2>
                                <input class="input is-medium" autocomplete="off" type="text" list="end-list"
                                       name="endStop" id="ending"/>
                                <datalist id="end-list">
                                    <c:forEach items="${sessionScope.listOfStops}" var="stop">
                                        <option><c:out value="${stop}"></c:out></option>
                                    </c:forEach>
                                </datalist>
                            </div>
                            <div class="field">
                            </div>
                            <div class="control ">
                                <input id="doneinput" type="hidden" name="imDone" value="false"/>
                                <input id="donebutton" type="submit" value="End Route"
                                       class="button is-link is-outlined"
                                       name="submit"/>
                                <input type="submit" value="Add a Transfer" class="button is-link is-outlined"
                                       name="addTransfer"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <c:url var="backgroundimg" value="/img/personwalkingyellow.jpg"></c:url>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#donebutton").click(function () {
            $("#doneinput").val(true);
        });
    });
</script>
<c:import url="/WEB-INF/jsp/footer.jsp"/>