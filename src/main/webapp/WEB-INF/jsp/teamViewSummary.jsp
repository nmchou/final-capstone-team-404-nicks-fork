<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:set var="pageTitle" value="teamViewSummary"/>
<c:url var="teamViewSummary" value="/teamViewSummary"/>

<form action="teamViewSummary" method="POST">
    <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
    <h2 class="title has-text-info"><span STYLE="font-family:Work Sans">What will you call this team?</span></h2>
    <input class="input is-medium" type="text"  id="routePicked" name="teamViewName"/>

    <div class="field">
        <div class="control">
            <input id="submitbutton" type="submit" value="Save" class="button is-link is-outlined"/>
        </div>
    </div>
</form>