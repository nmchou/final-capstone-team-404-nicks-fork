<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp"/>

<script type="text/javascript">
    $(document).ready(function () {

        $("form").validate({

            rules: {
                userName: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                confirmPassword: {
                    equalTo: "Passwords do not match"
                }
            },
            errorClass: "error"
        });
    });
</script>
<div class="container">
    <div class="level"></div>
    <div class="columns" id="index-container">
        <div class="column is-one-quarter">
        </div>
        <div class="column" id="middlecolumn">
            <c:url var="formAction" value="/login"/>
            <form method="POST" action="${formAction}">
                <input type="hidden" name="destination" value="${param.destination}"/>
                <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
                <div class="field">
                    <div class="has-text-centered title is-3 has-text-info">Log In</div>
                    <span style="font-family: 'Work Sans'" font-family:bold </span>
                    <label for="userName" class="label">User Name: <span style="font-family: 'Work Sans'"
                                                                         font-family:bold </span></label>
                    <div class="control">
                        <input type="text" id="userName" name="userName" placeHolder="User Name" class="input">
                    </div>
                </div>
                <div class="field">
                    <label for="password" class="label">Password: <span style="font-family: 'Work Sans'"
                                                                        font-family:bold </span></label>
                    <div class="control">
                        <input type="password" id="password" name="password" placeHolder="Password" class="input">
                    </div>
                </div>
                <div class="level">
                    <div class="level-item has-text-centered">
                        <div class="control">
                            <button type="submit" class="button">Login</button>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div class="control">
                            <button type="submit" class="button">Register</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <div class="column is-one-quarter">
        </div>
    </div>

    <c:url var="backgroundimg" value="/img/geometricimagecolor.jpg"></c:url>
    <style>
        body {
            height: 100%;
        }

        html {
            height: 100%
        }

        #outer-container {
            background-image: url("${backgroundimg}");
            background-size: contain;
            height: 80%;
        }

        .footer {
            background-color: unset;
            color: white;
        }

        #middlecolumn {
            background-color: white;
        }

        .login {
            position: relative;
            width: 100%;
            box-sizing: border-box;
            text-align: center;
            background-color: #fff;
            padding: 1.5rem;
        }

    </style>
</div>
<c:import url="/WEB-INF/jsp/footer.jsp"/>