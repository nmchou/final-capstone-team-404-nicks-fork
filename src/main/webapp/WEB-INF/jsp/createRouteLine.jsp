<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>

<c:url var="createRouteLine" value="/createRouteLine"/>

<section>
    <div class="columns">
        <div class="column">
            <c:url var="test" value="/img/templateyellowcircle.png"></c:url>
            <img src="${test}"/>
        </div>
        <div class="column">
            <c:import url="userSelections.jsp"></c:import>
            <form action="createRouteLine" method="POST">
                <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
                <div class="hero-body">
                    <h1 class="title">
                        <c:choose>
                            <c:when test="${currentRoute.beginStopName==null}">
                                Create a New Route
                            </c:when>
                            <c:otherwise>
                                Add a Transfer Line
                            </c:otherwise></c:choose></h1>
                    <div class="level">
                        <h2>Select a Subway Line</h2>
                    </div>
                    <div class="level">
                        <div class="buttons">
                            <c:forEach var="renderedLine" items="${listOfTrains}">
                                <label class="button subway ${renderedLine.style}"><input type="radio" name="trainLine"
                                                                                          value="${renderedLine.line}"
                                                                                          class="displayNone"/><c:out
                                        value="${renderedLine.line}"></c:out></label>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="field">
                        <div class="control">
                            <input type="submit" value="Choose Line" class="button is-link is-outlined"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>


<c:import url="/WEB-INF/jsp/footer.jsp"/>