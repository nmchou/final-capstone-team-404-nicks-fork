<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Train Track</title>
<c:url var="bulmaCss" value="/css/bulma/bulma.min.css" />
<c:url var="siteCss" value="/css/site.css" />
<c:url var="Subway" value="/css/Subway.css" />
	<c:url var="navbar" value="/css/navbar.css" />



	<link rel="stylesheet" type="text/css" href="${bulmaCss}">
<link rel="stylesheet" type="text/css" href="${siteCss}">
<link rel="stylesheet" type="text/css" href="${Subway}">
	<link rel="stylesheet" type="text/css" href="${navbar}">

	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

	<c:url var="jquery" value="/js/jquery.min.js"/>
	<script src="${jquery}" ></script>

<script type="text/javascript">

</script>

</head>
<body>
<div class="level is-small"></div>
<nav class="navbar" role="navigation" aria-label="main navigation">
	<div class="navbar">
		<c:url var="index" value="/"/>
		<a class="navbar-item"  href="${index}" id="largenavheading">
			Train Track
		</a>

		<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</a>
	</div>
	<c:url var="faqurl" value="/faq"/>
	<div id="navbarBasicExample" class="navbar-menu">
		<div class="navbar-end">
			<div class="navbar-item">
				<a href="${faqurl}">
				faq
			</a>
			</div>
			<a class="navbar-item" href="http://web.mta.info/maps/submap.html">
				MTA Info
			</a>
			<div class="navbar-item">
				<div class="buttons">
					<c:url var="loginurl" value="/login"/>
					<a href="${loginurl}">
						<c:url var="loginicon" value="/img/loginicon.png"/>
						<img src="${loginicon}"alt="login" height="42" width="42">
					</a>
				</div>
			</div>
		</div>
	</div>
</nav>


<div class="level is-small"></div>
	<%--<c:if test="${not empty currentUser}">--%>
		<%--<p id="currentUser">Current User: ${currentUser}</p>--%>
	<%--</c:if>--%>

	<section class = "section" id = "outer-container">

