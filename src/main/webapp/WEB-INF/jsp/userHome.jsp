<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:set var="pageTitle" value="userHome"/>
<%--<img class="img-circle" src="profilephoto.png">--%>

<div class="columns">
    <main class="column">
        <div class = "box">
        <div class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="title">Dashboard</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <button type="button" class="button is-small">
                        <p id="date"></p>
                        <script>
                            document.getElementById("date").innerHTML = Date();
                        </script>
                    </button>
                </div>
            </div>
        </div>
        </div>
        <div class="columns">
            <aside class="column  is-2">
                <div class = "box">
                <nav class="menu">
                    <p class="menu-label">
                        Administration
                    </p>
                    <ul class="menu-list">
                        <li>
                            My Routes
                            <ul>
                                <li>
                                    <c:url var="createRouteLine" value="/createRouteLine"/>
                                    <a href="${createRouteLine}">Create</a></li>
                                <li><a>View Details</a></li>
                                <li><a>View Private</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="">Manage Your Team</a>
                            <ul>
                                <li><c:url var="createNewTeam" value="/createNewTeam"/>
                                    <a href="${createNewTeam}">Create</a></li>
                                <li>
                                    <c:url var="addUser" value="/addUsersToTeam"/>
                                    <a href="${addUser}">Add an Employee</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                </div>

                <div class="column">
                    <div class = "box">
                    <h2>Real Time Delays</h2>
                    <div class="buttons is-static" style ="display:block">
                        <c:forEach items="${allTrainStatus}" var="trainStatus">
                            <c:choose>
                                <c:when test="${trainStatus.trainName == '1'}">
                                    <label class="button column subway red-line"><input type="radio" name="trainLine"
                                                                                        class="displayNone"/> 1 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == '2'}">
                                    <label class="button column subway red-line"><input type="radio" name="trainLine"
                                                                                        class="displayNone"/> 2 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == '3'}">
                                    <label class="button column subway red-line"><input type="radio" name="trainLine"
                                                                                        class="displayNone"/> 3 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == '4'}">
                                    <label class="button column subway green-line"><input type="radio" name="trainLine"
                                                                                          class="displayNone"/> 4 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == '5'}">
                                    <label class="button column subway green-line"><input type="radio" name="trainLine"
                                                                                          class="displayNone"/> 5 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == '6'}">
                                    <label class="button column subway green-line"><input type="radio" name="trainLine"
                                                                                          class="displayNone"/> 6 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == '7'}">
                                    <label class="button column subway purple-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> 7 </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'A'}">
                                    <label class="button column subway blue-line"><input type="radio" name="trainLine"
                                                                                         class="displayNone"/> A </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'C'}">
                                    <label class="button column subway blue-line"><input type="radio" name="trainLine"
                                                                                         class="displayNone"/> C </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'E'}">
                                    <label class="button column subway blue-line"><input type="radio" name="trainLine"
                                                                                         class="displayNone"/> E </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'N'}">
                                    <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> N </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'Q'}">
                                    <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> Q </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'R'}">
                                    <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> R </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'W'}">
                                    <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> W </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'B'}">
                                    <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> B </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'D'}">
                                    <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> D </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'F'}">
                                    <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> F </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'M'}">
                                    <label class="button column subway orange-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> M </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'R'}">
                                    <label class="button column subway yellow-line"><input type="radio" name="trainLine"
                                                                                           class="displayNone"/> R </label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>

                                <c:when test="${trainStatus.trainName == 'L'}">
                                    <label class="button column subway l-train"><input type="radio" name="trainLine"
                                                                                       class="displayNone"/>L</label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'J'}">
                                    <label class="button column subway brown-line"><input type="radio" name="trainLine"
                                                                                          class="displayNone"/>J</label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'Z'}">
                                    <label class="button column subway brown-line"><input type="radio" name="trainLine"
                                                                                          class="displayNone"/>Z</label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'G'}">
                                    <label class="button column subway g-train"><input type="radio" name="trainLine"
                                                                                       class="displayNone"/>G</label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                                <c:when test="${trainStatus.trainName == 'S'}">
                                    <label class="button column subway s-train"><input type="radio" name="trainLine"
                                                                                       class="displayNone">S</label>
                                    <c:out value="${trainStatus.trainStatus}"></c:out>
                                    <c:if test="${trainStatus.trainDirection!=null}">
                                        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
                                    </c:if>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                    </div>
                </div>
                </div>
            </aside>

            <div class="column">
                <div class="box">
                    <div class="heading">My Routes</div>
                    <table>
                        <tr>
                            <th>Name</th>
                        </tr>
                        <c:forEach items="${userRoutes}" var="routes">
                            <tr>
                                <td>
                                        <c:out value="${routes.routeName}"></c:out>
                                        <%--<c:forEach items="${routeDetailsForRoute}" var="routeDetailsForRoute">--%>
                                        <%--<c:if test="${routeDetailsForRoute.userRouteId == routes.userRouteId}">--%>
                                        <%--<c:out value="${routeDetailsForRoute}"></c:out>--%>
                                        <%--&lt;%&ndash;<c:forEach items="${routeDetails}" var = "routeDetails">&ndash;%&gt;--%>
                                        <%--&lt;%&ndash;<c:if test="${routeDetailsForRoute.userRouteId == routeDetails.userRouteId}">&ndash;%&gt;--%>
                                        <%--&lt;%&ndash;<c:out value="${routeDetails.beginStop}"></c:out>&ndash;%&gt;--%>
                                        <%--&lt;%&ndash;</c:if>&ndash;%&gt;--%>
                                        <%--&lt;%&ndash;</c:forEach>&ndash;%&gt;--%>
                                        <%--</c:if>--%>
                                        <%--</c:forEach>--%>
                            </tr>
                        </c:forEach>
                    </table>


                </div>

            </div>
            <div class="column">
                <div class = "box">
                <div class="heading">My Team Activity</div>
                <c:url var="teamview" value="/img/teamview.png"/>
                <img src="${teamview}" height="100%">
            </div>
            </div>
        </div>
    </main>

<%--</div>--%>
    <%--<div class="column white">--%>
            <%--<h2>Real Time Delays</h2>--%>
        <%--<div class="buttons is-static">--%>
            <%--<c:forEach items="${allTrainStatus}" var="trainStatus">--%>
                <%--<c:choose>--%>
                    <%--<c:when test="${trainStatus.trainName == '1'}">--%>
                        <%--<label class="button column subway red-line"><input type="radio" name="trainLine"--%>
                                                                            <%--class="displayNone"/> 1 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == '2'}">--%>
                        <%--<label class="button column subway red-line"><input type="radio" name="trainLine"--%>
                                                                            <%--class="displayNone"/> 2 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == '3'}">--%>
                        <%--<label class="button column subway red-line"><input type="radio" name="trainLine"--%>
                                                                            <%--class="displayNone"/> 3 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == '4'}">--%>
                        <%--<label class="button column subway green-line"><input type="radio" name="trainLine"--%>
                                                                              <%--class="displayNone"/> 4 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == '5'}">--%>
                        <%--<label class="button column subway green-line"><input type="radio" name="trainLine"--%>
                                                                              <%--class="displayNone"/> 5 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == '6'}">--%>
                        <%--<label class="button column subway green-line"><input type="radio" name="trainLine"--%>
                                                                              <%--class="displayNone"/> 6 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == '7'}">--%>
                        <%--<label class="button column subway purple-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> 7 </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'A'}">--%>
                        <%--<label class="button column subway blue-line"><input type="radio" name="trainLine"--%>
                                                                             <%--class="displayNone"/> A </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'C'}">--%>
                        <%--<label class="button column subway blue-line"><input type="radio" name="trainLine"--%>
                                                                             <%--class="displayNone"/> C </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'E'}">--%>
                        <%--<label class="button column subway blue-line"><input type="radio" name="trainLine"--%>
                                                                             <%--class="displayNone"/> E </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'N'}">--%>
                        <%--<label class="button column subway yellow-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> N </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'Q'}">--%>
                        <%--<label class="button column subway yellow-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> Q </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'R'}">--%>
                        <%--<label class="button column subway yellow-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> R </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'W'}">--%>
                        <%--<label class="button column subway yellow-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> W </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'B'}">--%>
                        <%--<label class="button column subway orange-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> B </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'D'}">--%>
                        <%--<label class="button column subway orange-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> D </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'F'}">--%>
                        <%--<label class="button column subway orange-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> F </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'M'}">--%>
                        <%--<label class="button column subway orange-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> M </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'R'}">--%>
                        <%--<label class="button column subway yellow-line"><input type="radio" name="trainLine"--%>
                                                                               <%--class="displayNone"/> R </label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>

                    <%--<c:when test="${trainStatus.trainName == 'L'}">--%>
                        <%--<label class="button column subway l-train"><input type="radio" name="trainLine"--%>
                                                                           <%--class="displayNone"/>L</label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'J'}">--%>
                        <%--<label class="button column subway brown-line"><input type="radio" name="trainLine"--%>
                                                                              <%--class="displayNone"/>J</label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'Z'}">--%>
                        <%--<label class="button column subway brown-line"><input type="radio" name="trainLine"--%>
                                                                              <%--class="displayNone"/>Z</label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'G'}">--%>
                        <%--<label class="button column subway g-train"><input type="radio" name="trainLine"--%>
                                                                           <%--class="displayNone"/>G</label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${trainStatus.trainName == 'S'}">--%>
                        <%--<label class="button column subway s-train"><input type="radio" name="trainLine"--%>
                                                                           <%--class="displayNone">S</label>--%>
                        <%--<c:out value="${trainStatus.trainStatus}"></c:out>--%>
                        <%--<c:if test="${trainStatus.trainDirection!=null}">--%>
                            <%--<c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>--%>
                        <%--</c:if>--%>
                    <%--</c:when>--%>
                <%--</c:choose>--%>
            <%--</c:forEach>--%>
        <%--</div>--%>
    <%--</div>--%>




<c:url var="backgroundimg" value="/img/bluetiles.jpg"></c:url>
<style>
    #outer-container {
        background-image: url("${backgroundimg}");
        background-size: contain;
    }

    .white {
        background-color: white;
    }


</style>
<c:import url="/WEB-INF/jsp/footer.jsp"/>