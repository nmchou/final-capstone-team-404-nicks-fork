<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/header.jsp" />

<script type="text/javascript">
	$(document).ready(function () {
		$.validator.addMethod('capitals', function(thing){
			return thing.match(/[A-Z]/);
		});
		$("form").validate({
			
			rules : {
				userName : {
					required : true
				},
				password : {
					required : true,
					minlength: 15,
					capitals: true,
				},
				confirmPassword : {
					required : true,		
					equalTo : "#password"  
				}
			},
			messages : {			
				password: {
					minlength: "Password too short, make it at least 15 characters",
					capitals: "Field must contain a capital letter",
				},
				confirmPassword : {
					equalTo : "Passwords do not match"
				}
			},
			errorClass : "error"
		});
	});
</script>
<div class = "columns">
	<div class = "column">
	</div>
	<div class = "column" id="middlecolumn">
		<c:url var="formAction" value="/login" />
		<form method="POST" action="${formAction}">
			<input type="hidden" name="destination" value="${param.destination}"/>
			<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
			<div class="field">
				<label for="userName" class="label">User Name: </label>
				<div class = "control">
					<input type="text" id="userName" name="userName" placeHolder="User Name" class="input">
				</div>
			</div>
			<div class="field">
				<label for="password"class="label">Password: </label>
				<div class = "control">
					<input type="password" id="password" name="password" placeHolder="Password" class="input">
					<span class = "icon is-small is-left">
						<i class = "fas fa-lock"></i>
					</span>
				</div>
			</div>
			<div class = "control">
				<button type="submit" class="button is-primary">Login</button>
			</div>
			<div class = "control">
				<button type="submit" class="button is-primary">Register</button>
			</div>
		</form>
	</div>
</form>
<c:url var="backgroundimg" value="/img/yellowpinkwall.jpg"></c:url>
<style>
	#outer-container {
		background-image: url("${backgroundimg}");
		background-size: contain;}

	.footer {
		background-color: unset;
		color: white;
	}
	#middlecolumn{
		background-color: white;
	}
</style>
<c:import url="footer.jsp"/>
<c:import url="/WEB-INF/jsp/footer.jsp" />

<c:url var="formAction" value="/users" />
<form method="POST" action="${formAction}">
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="form-group">
				<label for="userName">User Name: </label>
				<input type="text" id="userName" name="userName" placeHolder="User Name" class="form-control" />
			</div>
			<div class="form-group">
				<label for="password">Password: </label>
				<input type="password" id="password" name="password" placeHolder="Password" class="form-control" />
			</div>
			<div class="form-group">
				<label for="confirmPassword">Confirm Password: </label>
				<input type="password" id="confirmPassword" name="confirmPassword" placeHolder="Re-Type Password" class="form-control" />
			</div>
			<button type="submit" class="btn btn-primary">Create User</button>
		</div>
		<div class="col-sm-4"></div>
	</div>