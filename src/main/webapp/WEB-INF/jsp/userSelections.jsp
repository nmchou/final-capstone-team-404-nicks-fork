<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div>
    <c:if test="${userSelections!=null}">
        <div class="column">
            <div class="box">
                <div class="heading">Previously Selected Segments</div>

            <c:forEach items="${userSelections}" var="userSelection">
                <div><br></div>
                <div class="level-item"><c:out value="${userSelection.line} Train from ${userSelection.beginStopName} to ${userSelection.endStopName}"></c:out></div>
            </c:forEach>
            </div>
        </div>

    </c:if>
</div>
