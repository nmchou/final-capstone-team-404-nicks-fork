<%--
  Created by IntelliJ IDEA.
  User: Annie Reardon
  Date: 4/30/2019
  Time: 4:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="header.jsp"></c:import>
<c:url var="backgroundimg" value="/img/personwalkingyellow.jpg"></c:url>
<section class = "section parallax" style =" height: 45em; width: 100%; ">
    <div class = "container">
    <h1 class = "title is-size-1 has-text-centered future-title">The Future of Transportation.</h1>
    </div>
</section>

<%--<div class = "columns hasbackground">--%>
    <%--<div class = "column">--%>
        <%--column 1--%>
    <%--</div>--%>

    <%--<div class = "column">--%>
        <%--<div class="tile is-vertical notification is-info" >--%>
        <%--<c:url var="loginurl" value="/login"></c:url>--%>
            <%--<c:url var="registerurl" value="/users/new"></c:url>--%>
            <%--<a href = "${loginurl}" class = "button is-light" >--%>
            <%--Login--%>
        <%--</a>--%>
            <%--<a href = "${registerurl}" class = "button is-light" >--%>
                <%--register--%>
            <%--</a>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>

<div class = "hero-head">
    <div class = "hero-body" style ="padding-bottom: 10px;">
        <div class = "columns" style="text-align: center">
            <div class = "column">
                <h2 style = "font-size: 1.5rem; text-align:center"><span style="font-weight: bold">Real Time</span> <span style ="font-style: italic; font-family: 'Times New Roman'">Delays</span></h2>
                <h2 style="font-size: 15px; text-align:center; font-family: 'Work Sans'">Train statuses to inform you of the best route.</h2>
            </div>
            <div class = "column" >
                <h2 style = "font-size: 1.5rem; text-align:center;"><span style="font-weight: bold">Stay</span> <span style ="font-style: italic; font-family: 'Times New Roman'">Connected</span></h2>
                <h2 style ="font-size: 15px; text-align:center; font-family: 'Work Sans">Displays the status of your commute to friends, family, and co-workers.</h2>
            </div>
        </div>
    </div>
</div>
<hr class="style1">
<div class = "hero-head">
    <div class = "hero-body is-centered">
        <div class = "columns is-centered">
            <div class = "column is-half has-text-centered">
                <h1 style="text-align:center" class = "title">-</h1>
                <h2 style="text-align:center; font-weight: bold; font-family: 'Work Sans'">Designed with you in mind.</h2>
                <div class = "space"></div>

            </div>
        </div>
        <nav class="level" style ="color:#142b67">
            <div class="level-item has-text-centered">
                <div>
                    <i class="fas fa-seedling fa-2x"></i>
                    <br></br>
                    <p>Simple</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <i class="far fa-clock fa-2x"></i>
                    <br></br>
                    <p>Real Time</p>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <i class="fas fa-walking fa-2x"></i>
                    <br></br>
                    <h2>Step By Step Directions</h2>
                </div>
            </div>
            <div class="level-item has-text-centered">
                <div>
                    <i class="fas fa-exclamation fa-2x"></i>
                    <br></br>
                    <h2 class=>Receive Alerts</h2>
                </div>
            </div>
        </nav>
    </div>
</div>
<br></br>

<hr class="style1">
<br></br>
<div class = "hero-head">
    <div class = "hero-body">
        <div class = "columns">
            <div class = "column">
                <h2>Stay connected with us.</h2>
            </div>

            <div class = "column">
                <nav class="level" style ="color:#142b67">
                    <div class="level-item has-text-centered">
                        <div>
                            <i class="fab fa-facebook-f"></i>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <i class="fab fa-twitter"></i>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <i class="fab fa-pinterest-p"></i>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <i class="fab fa-instagram"></i>
                        </div>
                    </div>
                    <div class="level-item has-text-centered">
                        <div>
                            <i class="fab fa-youtube"></i>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


    <style>
        <%--#outer-container {--%>
<%--background-image: url("${backgroundimg}");--%>

        .parallax {
            /* The image used */
            background-image: url("${backgroundimg}");
            /* Full height */
            height: auto;

            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }


.footer {
    background-color: unset;
    color: white;
}
        .space {
            margin-bottom: 1cm;
        }
#middlecolumn{
    background-color: white;


}

        hr.style1{
            border-top: 1px solid #DCDCDC;
        }

        .future-title{
            color:white;
            margin-top: 25%;
        }
</style>


<c:import url="footer.jsp"></c:import>