<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>

<c:url var="tripsummary" value="tripsummary"/>

<div class="tile is-ancestor has-text-info">

    <div class="tile is-parent">
        <div class="tile is-child box">
            <p class="title has-text-info"><span STYLE="font-family:Work Sans">Complete Trip Details</span></p>
            <div class="field">
            </div>
            <h2 class="title has-text-info "><span STYLE="font-family:Work Sans">1. Access Level</span></h2>
            <form action="tripsummary" method="POST">
            <div class="control has-text-info" id="accessLevel">
                <label class="radio"><input type="radio" name="accessLevel" value="1">Public </label>
                <label class="radio"><input type="radio" name="accessLevel" value="2">Private </label>
            </div>
                <h1 class="title has-text-info"><span STYLE="font-family:Work Sans">2. Enter A Name</span></h1>

                <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}"/>
                <input class="input is-medium has-text-info" type="text" name="routeName"/>
                <div class="control ">
                    <input type="submit" value="Save" class="button is-link is-outlined" name="confirmRoute"/>
                </div>
            </form>
        </div>
        <div class="tile is-child box">
            <p class="title has-text-info"><span STYLE="font-family:Work Sans">Your Route Summary</span></p>
            <c:url var="tripsummary" value="tripsummary"/>


            <div class="timeline">
                <c:set var="routeIndex" value="0"/>
                <c:forEach var="route" items="${routeBuilderList}">
                <c:choose>
                    <c:when test="${routeIndex==0}">
                        <div class="timeline-header">
                            <span class="tag is-medium">Start</span>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="timeline-header">
                            <span class="tag is-medium is-primary">Transfer</span>
                        </div>
                    </c:otherwise>
                </c:choose>
                <div class="timeline-item">
                    <div class="timeline-marker"></div>
                    <div class="timeline-content">
                        <p class="heading"><c:out value="${route.line}"/> </p>
                    <p><c:out value="${route.beginStopName}"/></p>
                    </div>
                </div>
                    <c:if test="${routeIndex==lastRouteIndex}">
                    <div class="timeline-header">
                        <p class="heading"><span class="tag is-primary">End</span></p>

                        <p></p>
                    </div>
                <p><b><c:out value="${route.endStopName}"/></b></p>
            </div>
            </c:if>
            <c:set var="routeIndex" value="${routeIndex+1}"/>
            </c:forEach>
        </div>
    </div>

    <c:url var="backgroundimg" value="/img/bluetiles.jpg"></c:url>

    <style>
        #outer-container {
            background-image: url("${backgroundimg}");
            background-size: contain;
        }
    </style>


<c:import url="footer.jsp"></c:import>