<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/header.jsp"/>
<c:set var="pageTitle" value="trainStatus"/>
<c:url var="trainStatus" value="/trainStatus"/>


<h1 class="title">Train Status View</h1>
<p> Train Status Page</p>

<c:forEach items="${allTrainStatus}" var="trainStatus">
    <c:out value="${trainStatus.trainName}"></c:out>
    <c:out value="${trainStatus.trainStatus}"></c:out>
    <c:if test="${trainStatus.trainDirection!=null}">
        <c:out value="DIRECTION:${trainStatus.trainDirection}"></c:out>
    </c:if>


    </c:forEach>

<c:import url="footer.jsp"></c:import>