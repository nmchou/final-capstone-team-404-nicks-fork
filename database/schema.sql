-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;

CREATE TABLE app_user (
  id SERIAL PRIMARY KEY,
  user_name varchar(32) NOT NULL UNIQUE,
  password varchar(32) NOT NULL,
  role varchar(32),
  salt varchar(255) NOT NULL,
  login_attempts integer NOT NULL DEFAULT 0,
  manager_id INT,
  CONSTRAINT fk_manager_id FOREIGN KEY (manager_id) REFERENCES app_user (id)
);

CREATE TABLE stop (
  id varchar(8) PRIMARY KEY,
  stop_name varchar(64) NOT NULL,
  stop_lat float(8),
  stop_lon float(8),
  location_type INT,
  parent_station varchar(8),
  CONSTRAINT fk_parent_station FOREIGN KEY (parent_station) REFERENCES stop (id)
);

CREATE TABLE user_route (
  id SERIAL PRIMARY KEY,
  route_name varchar(64) NOT NULL,
  app_user_id INT NOT NULL,
  access_level INT,
  CONSTRAINT fk_app_user_id FOREIGN KEY (app_user_id) REFERENCES app_user (id)
);

CREATE TABLE route_details (
  id SERIAL PRIMARY KEY,
  user_route_id INT NOT NULL,
  begin_stop_id varchar(8) NOT NULL,
  end_stop_id varchar(8) NOT NULL,
  sequence_id SERIAL NOT NULL,
  CONSTRAINT fk_begin_stop_id FOREIGN KEY (begin_stop_id) REFERENCES stop (id),
  CONSTRAINT fk_end_stop_id FOREIGN KEY (end_stop_id) REFERENCES stop (id),
  CONSTRAINT fk_user_route_id FOREIGN KEY (user_route_id) REFERENCES user_route (id)
);

CREATE TABLE team_view (
  id SERIAL PRIMARY KEY,
  view_name varchar(64) NOT NULL,
  owner_id INT NOT NULL,
  access_level INT,
  CONSTRAINT fk_owner_id FOREIGN KEY (owner_id) REFERENCES app_user (id)
);

CREATE TABLE team_view_x_user_route (
  team_view_id INT NOT NULL,
  user_route_id INT NOT NULL,
  CONSTRAINT fk_team_view_id FOREIGN KEY (team_view_id) REFERENCES team_view (id),
  CONSTRAINT fk_user_route_id FOREIGN KEY (user_route_id) REFERENCES user_route (id)
);

CREATE TABLE line (
  id SERIAL PRIMARY KEY,
  name varchar(32) NOT NULL,
  direction varchar(32) NOT NULL,
  feed_id int
);

CREATE TABLE line_stops (
  line_id int,
  stop_id varchar(32),
  sequence_num int,
   CONSTRAINT fk_stop_id FOREIGN KEY (stop_id) REFERENCES stop (id),
   CONSTRAINT fk_line_id FOREIGN KEY (line_id) REFERENCES line (id),
   CONSTRAINT pk_line_stops PRIMARY KEY (line_id,stop_id) 
);
COMMIT;